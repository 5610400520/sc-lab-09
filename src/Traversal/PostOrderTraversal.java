package Traversal;
import java.util.ArrayList;
import java.util.List;

public class PostOrderTraversal implements Traversal
{
	private List<Node> list = new ArrayList<Node>();

	@Override
	public List<Node> traverse(Node node) 
	{
		if(node != null)
		{
			traverse(node.getLeft());
			traverse(node.getRight());
			list.add(node);
		}
		return list;
	}

}
