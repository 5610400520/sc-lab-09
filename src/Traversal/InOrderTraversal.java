package Traversal;
import java.util.ArrayList;
import java.util.List;

public class InOrderTraversal implements Traversal
{
	private List<Node> list = new ArrayList<Node>();

	@Override
	public List<Node> traverse(Node node) 
	{
		if(node != null)
		{
			traverse(node.getLeft());
			list.add(node);
			traverse(node.getRight());
		}
		return list;
	}



}
