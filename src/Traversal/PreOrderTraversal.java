package Traversal;
import java.util.ArrayList;
import java.util.List;

public class PreOrderTraversal implements Traversal
{
	private List<Node> list = new ArrayList<Node>();
	
	@Override
	public List<Node> traverse(Node node) 
	{   
		if(node != null)
		{
			list.add(node);
			traverse(node.getLeft());
			traverse(node.getRight());
		}
		return list;
	}

}
