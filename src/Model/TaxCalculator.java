package Model;

import java.util.ArrayList;

import Interface.Taxable;

public class TaxCalculator {
	public static double sum(ArrayList<Taxable> taxList){
		double sum = 0;
		for(Taxable taxableObj:taxList){
			sum += taxableObj.getTax();
		}
		return sum;
	}
}
